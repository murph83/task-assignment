Call Center Audit Task Assignment
=================================

Importing the project
---------------------
This project is designed to be layerd on top of the [Travel Agency Demo](https://github.com/jbossdemocentral/bpms-travel-agency-demo).
Please install that environment according to the instructions found there.

Once the workbench is started, log in with an admin user - either bpmsAdmin or erics - and navigate to the Authoring > Administration tab.
![Authoring>Administration]
(docs/Authoring>Administration.png)

Click on Repositories and select Clone Repository
![Repositories>Clone]
(docs/Repositories>Clone.png)

Fill in the Repository Information:

|Field | Value |
|----- | ----- |
|Repository Name | task-assignment |
|Organizational Unit | Demos |
|Git URL | https://gitlab.com/murph83/task-assignment.git |
|User Name | {leave blank} |
|Password | {leave blank} |


Click Clone
![Repository Information]
(docs/Repository Information.png)


Running the demo
----------------
Once the repository has finished cloning, open the Project Editor for the taskassignment project by going to the Authoring > Project Authoring tab and clicking Open Project Editor
![Open Project Editor]
(docs/Open Project Editor.png)

In the Build menu, select Build & Deploy 

Once the Build is complete, navigate to Process Management > Process Definitions

Start the load-demo process, this will create three call records, ready for auditing. You can accept the default data, or make up some of your own
![Load Demo]
(docs/Load Demo.png)

The assign-audit process is on a 300s timer. 5 minutes after the project is deployed and every 5 minutes after that, it will kick off. This can be adjusted in the start node of the process definition
![Time Cycle]
(docs/Time Cycle.png)

Once the assign-audit process has kicked off, you should see a logging message that indicates it has found 3 tasks in need of assignment. 

While logged in as a user in the reviewerrole group - erics is the only user configured in this group by default - navigate to the Tasks tab

Select the Assign Audit Tasks task, claim and complete it. Assign any defined user as the auditor for each task. For simplicity, we select erics.
![Assign Audits]
(docs/Assign Audits.png)

The three Audit Tasks should now appear in the task list for the user selected. Perform the Audit by opening the task and clicking start. Fill out the form and click complete.
![Perform Audit]
(docs/Perform Audit.png)