package rh.demo.taskassignment;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class TaskAssignmentList implements java.io.Serializable
{

   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label(value = "Task Assignments")
   private java.util.List<rh.demo.taskassignment.TaskAssignment> taskAssignments;

   public TaskAssignmentList()
   {
   }

   public java.util.List<rh.demo.taskassignment.TaskAssignment> getTaskAssignments()
   {
      return this.taskAssignments;
   }

   public void setTaskAssignments(
         java.util.List<rh.demo.taskassignment.TaskAssignment> taskAssignments)
   {
      this.taskAssignments = taskAssignments;
   }

   public TaskAssignmentList(
         java.util.List<rh.demo.taskassignment.TaskAssignment> taskAssignments)
   {
      this.taskAssignments = taskAssignments;
   }

}