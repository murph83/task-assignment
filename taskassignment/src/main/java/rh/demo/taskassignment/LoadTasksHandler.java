package rh.demo.taskassignment;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LoadTasksHandler implements WorkItemHandler {

    private static final Logger LOG = LoggerFactory.getLogger(LoadTasksHandler.class);

    private EntityManagerFactory emf;
    private EntityManager em;
    private ClassLoader classloader;

    public LoadTasksHandler(String persistenceUnit, ClassLoader cl) {
        ClassLoader tccl = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(cl);
            this.emf = Persistence.createEntityManagerFactory(persistenceUnit);

        } finally {
            Thread.currentThread().setContextClassLoader(tccl);
        }
        em = emf.createEntityManager();
        this.classloader = cl;
    }

    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

        Map<String, Object> outputMap = new HashMap<>();

        List<AuditTask> tasks = em.createQuery("SELECT a FROM AuditTask a WHERE a.auditorsRating IS NULL", AuditTask.class).getResultList();

        TaskAssignmentList taskAssignments =
                tasks.stream()
                        .map(t -> new TaskAssignment(t, null))
                        .collect(Collectors.collectingAndThen(
                                Collectors.toList(),
                                list -> new TaskAssignmentList(list)));

        LOG.info("Found {} tasks needing assignment", taskAssignments.getTaskAssignments().size());
        for(TaskAssignment ta : taskAssignments.getTaskAssignments()){
            LOG.info("  {}:{}", ta.getTask().getCallId(), ta.getTask().getCounselorId());
        }

        outputMap.put("tasks", taskAssignments);

        manager.completeWorkItem(workItem.getId(), outputMap);
    }

    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        manager.abortWorkItem(workItem.getId());
    }
}
